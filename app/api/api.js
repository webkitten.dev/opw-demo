const { NODE_ENV } = process.env
const os = require('os')
const fs = require('fs-extra')
const path = require('path')
const nrc = require('node-run-cmd')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const reload = require('express-reload')

const shortcuts = require('./api/core/shortcuts')
const tray = require('./api/core/tray')

const snippetsWatcher = require('./api/watchers/snippets')
const sublimeSettingsWatcher = require('./api/watchers/sublime-settings')

const { notify } = require('./api/commands')

const app = express()
app.use(bodyParser.json({ limit: "999mb" }))
app.use(bodyParser.urlencoded({ limit: "999mb", extended: true, parameterLimit: 50000 }))
app.use(cors())
app.use('/images', express.static(path.join(os.homedir(), 'one-punch-web', 'sources', 'cheatsheets', 'data')))
app.use('/libs', express.static(path.join(os.homedir(), 'one-punch-web', 'sources', 'libs')))

const routesPath = path.join(os.homedir(), 'one-punch-web', 'app', 'api', 'routes')
fs.readdirSync(routesPath)
  .map(file => (NODE_ENV !== 'dev'
    ? app.use(require(path.join(routesPath, file)))
    : app.use(reload(path.join(routesPath, file)))))




snippetsWatcher.init()
sublimeSettingsWatcher.init()

tray.beforeInit()

app.listen(6666, () => {
  console.log('api listening on port 6666')
  const command = NODE_ENV !== 'dev'
    ? 'node node_modules/nuxt/bin/nuxt start app/'
    : 'node node_modules/nuxt/bin/nuxt app/'
  const terminal = nrc.run(command, {
    onData: (data) => {
      if (NODE_ENV === 'dev')  {
        if (data.includes('Builder initialized')) {
          notify('Builder initialized')
        }
        if (data.includes('Memory usage')) {
          setTimeout(() => {
            tray.init()
            shortcuts.init()
            notify('App is ready', null, 'success.png')
          }, 3000)
        }
      } else {
        if (data.includes('Listening on:')) {
          setTimeout(() => {
            tray.init()
            shortcuts.init()
            notify('App is ready', null, 'success.png')
          }, 3000)
        }
      }
      console.log(data)
    },
    onError: (error) => {
      console.log(error)
    },
    onDone: (code) => {
      console.log(code)
    }
  }, {
    cwd: process.cwd(),
    shell: true
  })
})


