const { closeTray } = require('./core/tray')
const shell = require('shelljs')

const defaultWindowSettings = {
  icon: './app/static/app.png',
  position: 'center',
  width: 1215,
  min_width: 800,
  height: 630,
  min_height: 630,
  show: true,
  show_in_taskbar: true,
  frame: false,
  transparent: false,
  resizable: true,
  always_on_top: true,
}

const shellexec = (command) => new Promise(resolve => {
  if (command !== '') {
    shell.exec(command, { async: true }, () => resolve())
    return
  }
  resolve()
})

module.exports = {
  openOpwMain() {
    nw.Window.open('http://localhost:6767', {
      ...defaultWindowSettings,
    })
  },
  openTranslate() {
    nw.Window.open('http://localhost:6767?tab=translate', {
      ...defaultWindowSettings,
    })
  },
  openAsana() {
    nw.Window.open('http://localhost:6767/asana', {
      ...defaultWindowSettings,
    })
  },
  openTerminal() {
    nw.Window.open('http://localhost:6767/terminal', {
      ...defaultWindowSettings,
    })
  },
  openSettings() {
    nw.Window.open('http://localhost:6767/settings', {
      ...defaultWindowSettings,
    })
  },
  openOpwLocal(pathToOpen) {
    nw.Window.open(`http://localhost:6767/local?path=${pathToOpen}`, {
      ...defaultWindowSettings,
    })
  },
  openOpwCheatsheet(pathToOpen) {
    nw.Window.open(`http://localhost:6767/cheatsheet?path=${pathToOpen}`, {
      ...defaultWindowSettings,
    })
  },
  openRadio() {
    nw.Window.getAll(windows => {
      const radioWin = windows.filter(item => item.title === 'Radio')
      if (!radioWin.length) {
        nw.Window.open('http://localhost:6767/radio', {
          ...defaultWindowSettings,
          title: 'Radio',
        })
      } else {
        radioWin[0].hide()
        setTimeout(() => {
          radioWin[0].show()
        }, 1000);
      }
    })
  },
  notify(text, title, icon) {
    const n = new Notification(title || 'One Punch Web', {
      icon: `./app/static/${icon || 'app.png'}`,
      body: text
    })
    setTimeout(() => {
      n.close.bind(n)
      n.close()
    }, 4000)
  },
  shellexec,
  closeApp() {
    nw.App.closeAllWindows()
    closeTray()
    nw.App.quit()
  },
}
