const path = require('path')
const os = require('os')
const open = require('open')
const {
  openOpwMain,
  openAsana,
  openTerminal,
  openTranslate,
  shellexec,
} = require('../commands')


module.exports = {
  init() {
    const altCtrlW = new nw.Shortcut({
      key: 'Alt+Ctrl+W',
      active() {
        openOpwMain()
      }
    })
    nw.App.registerGlobalHotKey(altCtrlW)

    const altCtrlA = new nw.Shortcut({
      key: 'Alt+Ctrl+A',
      active() {
        openAsana()
      }
    })
    nw.App.registerGlobalHotKey(altCtrlA)

    const altCtrlHome = new nw.Shortcut({
      key: 'Alt+Ctrl+Home',
      active() {
        open(path.join(os.homedir(), 'one-punch-web'))
      }
    })
    nw.App.registerGlobalHotKey(altCtrlHome)

    const altCtrlT = new nw.Shortcut({
      key: 'Alt+Ctrl+T',
      active() {
        openTerminal()
      }
    })
    nw.App.registerGlobalHotKey(altCtrlT)

    const altCtrlG = new nw.Shortcut({
      key: 'Alt+Ctrl+G',
      active() {
        openTranslate()
      }
    })
    nw.App.registerGlobalHotKey(altCtrlG)

    const altCtrlPageUp = new nw.Shortcut({
      key: 'Alt+Ctrl+PageUp',
      active: () => {
        const command = os.platform() === 'win32'
          ? `"${path.join(os.homedir(), 'one-punch-web', 'deps', 'nircmd', 'nircmd.exe')}" changesysvolume 2000 && exit`
          : `/bin/bash -c "amixer -D pulse sset Master 5%+"`
        shellexec(command)
      }
    })
    nw.App.registerGlobalHotKey(altCtrlPageUp)

    const altCtrlPageDown = new nw.Shortcut({
      key: "Alt+Ctrl+PageDown",
      active() {
        const command = os.platform() === 'win32'
          ? `"${path.join(os.homedir(), 'one-punch-web', 'deps', 'nircmd', 'nircmd.exe')}" changesysvolume -2000 && exit`
          : `/bin/bash -c "amixer -D pulse sset Master 5%-"`
        shellexec(command)
      }
    })
    nw.App.registerGlobalHotKey(altCtrlPageDown)

    const muteSwitch = new nw.Shortcut({
      key: "Alt+Ctrl+-",
      active() {
        const command = os.platform() === 'win32'
          ? `"${path.join(os.homedir(), 'one-punch-web', 'deps', 'nircmd', 'nircmd.exe')}" mutesysvolume 2 && exit`
          : `/bin/bash -c "amixer -D pulse set Master Playback Switch toggle"`
        shellexec(command)
      }
    })
    nw.App.registerGlobalHotKey(muteSwitch)
  }
}