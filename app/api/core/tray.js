const win = nw.Window.get()

module.exports = {
  tray: null,
  placeholder: null,
  beforeInit() {
    this.placeholder = new nw.Tray({
      title: 'One-Punch-Web',
      icon: './app/static/app-disabled.png'
    })

    this.placeholder.menu = new nw.Menu()

    this.placeholder.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Dev tools',
      click: () => {
        win.showDevTools()
      }
    }))

    this.placeholder.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Quit',
      click: () => {
        nw.App.closeAllWindows()
        this.placeholder.remove()
        nw.App.quit()
      }
    }))
  },
  init() {
    this.placeholder.remove()
    this.tray = new nw.Tray({
      title: 'One-Punch-Web',
      icon: './app/static/app.png'
    })

    this.tray.menu = new nw.Menu()

    this.tray.on('click', () => {
      const { openOpwMain } = require('../commands')
      openOpwMain()
    })

    this.tray.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Asana',
      click: () => {
        const { openAsana } = require('../commands')
        openAsana()
      }
    }))

    this.tray.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Radio',
      click: () => {
        const { openRadio } = require('../commands')
        openRadio()
      }
    }))

    this.tray.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Settings',
      click: () => {
        const { openSettings } = require('../commands')
        openSettings()
      }
    }))

    this.tray.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Dev tools',
      click: () => {
        win.showDevTools()
      }
    }))

    this.tray.menu.append(new nw.MenuItem({
      type: 'normal',
      label: 'Quit',
      click: () => {
        nw.App.closeAllWindows()
        this.tray.remove()
        nw.App.quit()
      }
    }))
  },
  closeTray() {
    this.tray.remove()
  },
}