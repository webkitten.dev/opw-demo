const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const open = require('open')
const browserBookmarks = require('browser-bookmarks')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

router.get('/bookmarks/get', (req, res, next) => {
  browserBookmarks.getChrome().then(bookmarks => {
    res.json(
      bookmarks.map(item => {
        return {
          name: item.title,
          url: item.url,
          icon: item.favicon,
          folder: item.folder
        }
      })
    )
  })
})

module.exports = router