const { Router } = require('express')
const YandexDisk = require('yandex-disk').YandexDisk
const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const config = require(path.join(os.homedir(), 'one-punch-web', 'config.json'))

const router = Router()

router.get('/books/get', (req, res, next) => {
  const disk = new YandexDisk(config.yandexDisk.login, config.yandexDisk.password)
  const localBooks = fs.readdirSync(path.join(os.homedir(), 'one-punch-web', 'sources', 'books'))
  disk.readdir('./_Books/', (err, books) => {
    res.json(books.map(book => {
      return {
        ...book,
        name: book.displayName,
        size: (book.size / (1024*1024)).toFixed(1) + 'mb',
        local: localBooks.includes(book.displayName)
      }
    }))
  })
})

module.exports = router