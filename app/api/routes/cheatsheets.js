const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const open = require('open')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const cheatsheetsPath = path.join(os.homedir(), 'one-punch-web', 'sources', 'cheatsheets')

router.get('/cheatsheets/get', (req, res, next) => {
  const cheatsheetsData = fs
    .readdirSync(path.join(cheatsheetsPath, 'data'))
  const cheatsheets = fs
    .readdirSync(cheatsheetsPath)
    .filter(item => path.extname(item) === '.md')
    .map(item => {
      const name = path.basename(item, '.md')
      return {
        name,
        data: cheatsheetsData.includes(name)
      }
    })
  res.json(cheatsheets)
})

module.exports = router