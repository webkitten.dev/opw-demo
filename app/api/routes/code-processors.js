const { Router } = require('express')
const {
  notify,
} = require('../commands')
const detective = require('detective')
const isBuiltInModule = require('is-builtin-module')
const beautify = require('js-beautify').js_beautify
const jsmin = require('jsmin').jsmin
const babel = require('babel-core')
const less = require('less')
const autoprefixer = require('autoprefixer')
const postcss = require('postcss')
const removePrefixes = require('postcss-remove-prefixes')
const perfectionist = require('perfectionist')
const mincss = require('min.css')
const php2pug = require('php2pug')
const stt = require('spaces-to-tabs')
const pug = require('pug')
const cheerio = require('cheerio')
const _ = require('lodash')
const pretty = require('pretty')
const minify = require('html-minifier').minify

const router = Router()

const tabulationResult = (inputCode, resultCode) => {
  const array = inputCode.split('\n')
  let tabulation = array[0]
  array.forEach((item) => {
    const itemSplit = item.split('')
    const tabltnSplit = tabulation.split('')
    if (item !== '') {
      tabulation = ''
      let stop = false
      for (let i = 0; i < tabltnSplit.length; i++) {
        if (tabltnSplit[i] === itemSplit[i] && tabltnSplit[i] === ' ' && !stop) {
          tabulation += tabltnSplit[i]
        } else {
          stop = true
        }
      }
    }
  })
  const resultCodeSplit = resultCode.split('\n')
  const resArr = []
  resultCodeSplit.forEach((item) => {
    if (item.trim() !== "") {
      resArr.push(tabulation + item)
    }
  })
  return resArr.join('\n')
}

router.post('/code-processor/js-parse-npm-packages', (req, res, next) => {
  const parsed = detective(req.body.code)
    .filter((module) => !isBuiltInModule(module) && module !== 'nw.gui')
    .join(' ')
  const result = `npm i ${parsed} --save`
  const clipboard = nw.Clipboard.get()
  clipboard.set(result, 'text')
  notify(result, 'packages copied to clipboard', 'success.png')
  res.json({
    success: true
  })
})

router.post('/code-processor/js-beautify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to beautify', 'js beautify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const beautified = beautify(code, {
        indent_size: 2,
        space_after_anon_function: true,
        space_in_paren: false,
        preserve_newlines: false,
        keep_array_indentation: true
      })
      notify(beautified, 'js beautify success', 'success.png')
      res.json({
        result: beautified
      })
    } catch (e) {
      notify(e, 'js beautify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/js-minify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to minify', 'js minify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const min = jsmin(code, 3).trim()
      const minified = tabulationResult(code, min)
      notify(minified, 'js minify success', 'success.png')
      res.json({
        result: minified
      })
    } catch (e) {
      notify(e, 'js minify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/js-es6-es5', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to minify', 'js es6-es5 error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const transformed = babel.transform(code, { presets: ['es2015'] })
      const result = tabulationResult(code, transformed.code.replace('"use strict";', '').replace("'use strict';", '').trim())
      notify(result, 'js es6-es5 success')
      res.json({
        result
      })
    } catch (e) {
      notify(e, 'js es6-es5 error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/css-less2css', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to less2css', 'css less2css error', 'error.png')
    res.json({
      result: code
    })
  } else {
    const lessCode = code.replace(/\t/g, '  ')
    less.render(lessCode).then((output) => {
      const result = tabulationResult(lessCode, output.css)
      notify(result, 'css less2css success', 'success.png')
      res.json({
        result
      })
    }, (e) => {
      notify(e, 'css less2css error', 'error.png')
      res.json({
        result: code
      })
    })
  }
})

router.post('/code-processor/css-autoprefix', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to autoprefix', 'css autoprefix error', 'error.png')
    res.json({
      result: code
    })
  } else {
    postcss([autoprefixer]).process(code).then((output) => {
      notify(output.css, 'css autoprefix success', 'success.png')
      res.json({
        result: output.css
      })
    }, (e) => {
      notify(e, 'css autoprefix error', 'error.png')
      res.json({
        result: code
      })
    })
  }
})

router.post('/code-processor/css-remove-prefixes', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to remove', 'css remove prefixes error', 'error.png')
    res.json({
      result: code
    })
  } else {
    postcss([ removePrefixes() ]).process(code).then((output) => {
      notify(output.css, 'css remove prefixes success', 'success.png')
      res.json({
        result: tabulationResult(code, output.css)
      })
    }, (e) => {
      notify(e, 'css remove prefixes error', 'error.png')
      res.json({
        result: code
      })
    })
  }
})

router.post('/code-processor/css-beautify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to beautify', 'css beautify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    const css = code.replace(/\t/g, "  ")
    perfectionist.process(css, {
      indentSize: 2,
      cascade: false,
      colorCase: "upper",
      trimLeadingZero: false
    }).then(beauty => {
      res.json({
        result: beauty.css
      })
    }, (e) => {
      notify(e, 'css beautify error', 'error.png')
      res.json({
        result: code
      })
    })
  }
})


router.post('/code-processor/css-minify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to minify', 'css minify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const min = mincss(code)
      res.json({
        result: tabulationResult(code, min)
      })
    } catch (e) {
      notify(e, 'css minify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/css-oneline', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to oneline', 'css oneline error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const array = code.split('\n')
      const preResArr = []
      let newLine = ""
      for (let i in array) {
        var line = array[i]
        if (line.indexOf("{") !== -1 && line.indexOf('}') !== -1) {
          preResArr.push(line)
        } else if (line.indexOf('{') !== -1) {
          if (line.indexOf('@media') !== -1) {
            newLine = ' ' + line.replace('\r', '').trim() + ' '
            preResArr.push(newLine)
            newLine = ''
          } else {
            newLine += ' ' + line.replace('\r', '').trim() + ' '
          }
        } else if (line.indexOf("}") !== -1) {
          preResArr.push(newLine + ' ' + line.replace('\t', '').trim())
          newLine = ""
        } else if (line !== '') {
          newLine += line.replace('\r', '').trim()
        }
      }

      res.json({
        result: tabulationResult(code, preResArr.join('\n'))
      })
    } catch (e) {
      notify(e, 'css oneline error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/css-px2rem', (req, res, next) => {
  const code = req.body.code
  const pxToRem = (str) => {
    try {
      const px = parseInt(str.replace(/px/gi, ''))
      return parseFloat((px / parseInt(16, 10)).toPrecision(4)) + 'rem'
    } catch (e) {
      notify(e, 'css px2rem error', 'error.png')
      res.json({
        result: code
      })
    }
  }
  const remToPx = (str) => {
    try {
      const rem = str.replace(/rem/gi, '')
      return (rem * 16).toFixed(0) + 'px'
    } catch (e) {
      notify(e, 'css px2rem error', 'error.png')
      res.json({
        result: code
      })
    }
  }
  if (code.length === 0) {
    notify('nothing to px2rem', 'css px2rem error', 'error.png')
    res.json({
      result: code
    })
  } else if (isNaN(parseInt(code))) {
    notify('wrong format', 'css px2rem error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const codeToLower = code.toLowerCase()
      if (codeToLower.includes('px')) {
        res.json({ result: pxToRem(codeToLower) })
      } else if (codeToLower.includes('rem')) {
        res.json({ result: remToPx(codeToLower) })
      } else {
        res.json({ result: pxToRem(codeToLower) })
      }
    } catch (e) {
      notify(e, 'css px2rem error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/html-html2pug', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to html2pug', 'html html2pug error', 'error.png')
    res.json({
      result: code
    })
  } else {
    const generatePug = php2pug(code)
    generatePug.then((pugText) => {
      const pugTextSplit = pugText.split('\n')
      const resArr = []
      pugTextSplit.forEach((item) => {
        const toTabs = stt(item, 4)
        const ir = toTabs.replace(/\t/g, '  ')
        resArr.push(ir);
      })
      const result = resArr.join('\n')
      if (result !== '' && htmlOrPhp !== '') {
        notify(prettyHtml, 'html html2pug success')
        res.json({
          result: tabulationResult(code, result)
        })
      } else {
        notify(e, 'html html2pug error', 'error.png')
        res.json({
          result: code
        })
      }
    }, (e) => {
      notify(e, 'html html2pug error', 'error.png')
      res.json({
        result: code
      })
    })
  }
})


router.post('/code-processor/html-pug2html', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to pug2html', 'pug2html minify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const fn = pug.compile(code, { pretty: true })
      const html = fn()
      const result = html.split('\n').slice(1).join('\n')
      notify(result, 'html pug2html success', 'success.png')
      res.json({
        result
      })
    } catch(e) {
      notify(e, 'html pug2html error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})


router.post('/code-processor/html-cssrules', (req, res, next) => {
  const code = req.body.code
  // need parent inline
  const inlineArr=['b','big','i','small','tt','abbr','acronym','cite','code','dfn','em','kbd','strong','samp','time','var','a','bdo','br','img','map','object','q','script','span','sub','sup','button','input','label','select','textarea','h1','h2','h3','h4','h5','h6','ol','p','audio','blockquote','canvas','li','ul']
  // dont need block parent
  const blockArr=['address','article','aside','dd','div','dl','fieldset','figcaption','figure','footer','header','hgroup','hr','main','nav','noscript','output','pre','section','tfoot','video']
  // need parent and block
  const inlineBlock = ['table', 'ul', 'form']
  // ignored
  const ignoredArr=['script','link','noscript','body','html','meta','title','head']
  if (code.length === 0) {
    notify('nothing to cssrules', 'html cssrules error', 'error.png')
    res.json({
      success: false
    })
  } else {
    try {
      const $ = cheerio.load(code, { decodeEntities: false })
      const arr = []
      let parentNow = ''
      $('*').each(function() {
        const self = $(this)
        const id = self.attr('id')
        const className = self.attr('class')
        const tagName = self.prop('tagName').toLowerCase()
        if (ignoredArr.indexOf(tagName) === -1) {
          let named = false
          let hasId = true
          if (id !== undefined) {
            named = true
            hasId = false
            arr.push({ tagName, cssPath: `#${id} {}` })
            parentNow = `#${id} `
            if (tagName === 'a' || tagName === 'button') {
              arr.push({ tagName, cssPath: `#${id}:hover {}` })
              arr.push({ tagName, cssPath: `#${id}:focus {}` })
              arr.push({ tagName, cssPath: `#${id}:visited {}` })
            }
          }
          if (className && hasId) {
            const splitClass = className.split(' ')
            let toPush = ''
            splitClass.forEach((item) => {
              if (item.slice(0, 4) !== 'col-' && item !== 'container' && item !== 'row' && item !== 'container-fluid' && item !== 'row-fluid') toPush += '.' + item
            })
            let blockParent = ''
            if (toPush !== '') {
              arr.push({ tagName, cssPath: blockParent + toPush + ' {}' })
              if (tagName === 'a' || tagName === 'button') {
                arr.push({ tagName, cssPath: `${blockParent + toPush}:hover {}` })
                arr.push({ tagName, cssPath: `${blockParent + toPush}:focus {}` })
                arr.push({ tagName, cssPath: `${blockParent + toPush}:visited {}` })
              }
              named = true
              parentNow = `${toPush} `
            }
          }
          if (!named && blockArr.indexOf(tagName) === -1 && inlineArr.indexOf(tagName) !== -1) {
            arr.push({ tagName, cssPath: `${parentNow + tagName} {}` })
            if (tagName === 'a' || tagName === 'button') {
              arr.push({ tagName, cssPath: `${parentNow + tagName}:hover {}` })
              arr.push({ tagName, cssPath: `${parentNow + tagName}:focus {}` })
              arr.push({ tagName, cssPath: `${parentNow + tagName}:visited {}` })
            }
          }
        }
      })
      const resArr = _.uniqBy(arr, 'cssPath')
      const result = resArr.map(item => item.cssPath).join('\n')
      const clipboard = nw.Clipboard.get()
      clipboard.set(result, 'text')
      notify(result, 'rules copied to clipboard', 'success.png')
      res.json({
        success: true
      })
    } catch (e) {
      notify(e, 'html cssrules error', 'error.png')
      res.json({
        success: false
      })
    }
  }
})


router.post('/code-processor/html-beautify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to minify', 'html beautify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const prettyHtml = pretty(code)
      notify(prettyHtml, 'html beautify success', 'success.png')
      res.json({
        result: tabulationResult(code, prettyHtml)
      })
    } catch (e) {
      notify(e, 'html beautify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})


router.post('/code-processor/html-minify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to minify', 'html minify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const min = minify(code, {
        collapseInlineTagWhitespace: true,
        collapseWhitespace: true,
        removeComments: true
      })
      notify(min, 'html minify success', 'success.png')
      res.json({
        result: tabulationResult(code, min)
      })
    } catch (e) {
      notify(e, 'html minify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})

router.post('/code-processor/php-beautify', (req, res, next) => {
  const code = req.body.code
  if (code.length === 0) {
    notify('nothing to beautify', 'php minify error', 'error.png')
    res.json({
      result: code
    })
  } else {
    try {
      const beautified = beautify(code, {
        indent_size: 4,
        "space_after_anon_function": true,
        "space_in_paren": false,
        "preserve_newlines": false,
        "keep_array_indentation": false
      })
      const result = beautified
        .replace(/ - > /g, '->')
        .replace(/[\.]\n[\s]+/g, '.')
        .replace(/[.][\s]+=/g, ' .=')
        .replace(/public[\s]?\n[\s]+/g, 'public ')
        .replace(/[.]\s=/g, 'public ')
      notify(result, 'php beautify success', 'success.png')
      res.json({
        result
      })
    } catch (e) {
      notify(e, 'php beautify error', 'error.png')
      res.json({
        result: code
      })
    }
  }
})


module.exports = router