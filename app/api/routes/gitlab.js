const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const open = require('open')
const jsonfile = require('jsonfile')
const _gitlab = require('gitlab')
const { Router } = require('express')
const config = require(path.join(os.homedir(), 'one-punch-web', 'config.json'))
const {
  notify,
} = require('../commands')

const router = Router()

const api = new _gitlab.Gitlab({
  host: config.gitlab.host,
  token: config.gitlab.token,
})

router.get('/gitlab-repos/get', async (req, res, next) => {
  const gitlabRepos = await api.Projects.all({
    membership: true,
    page: 1,
    per_page: 100,
    showPagination: true
  })
  res.json(gitlabRepos)
})

router.post('/gitlab-repos/create', async (req, res, next) => {
  const newGitlabRepoName = req.body.newName
  const repo = await api.Projects.create({
    name: newGitlabRepoName,
    visibility: 'private',
  })
  res.json(repo)
})

router.post('/gitlab-repos/rename', async (req, res, next) => {
  const gitlabRepo = req.body.repo
  const newGitlabRepoName = req.body.newName
  const repo = await api.Projects.edit(gitlabRepo.id, {
    name: newGitlabRepoName,
    path: newGitlabRepoName,
  })
  res.json(repo)
})

router.post('/gitlab-repos/fork', async (req, res, next) => {
  const gitlabRepo = req.body.repo
  const newGitlabRepoName = req.body.newName
  const repo = await api.Projects.fork(gitlabRepo.id, {
    name: newGitlabRepoName,
    path: newGitlabRepoName,
  })
  res.json(repo)
})

router.post('/gitlab-repos/delete', (req, res, next) => {
  const gitlabRepo = req.body.repo
  api.Projects.remove(gitlabRepo.id)
  res.json({
    success: true
  })
})


module.exports = router