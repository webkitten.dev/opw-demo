const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const rimraf = require('rimraf')
const open = require('open')
const jsonfile = require('jsonfile')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const libsDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'libs')

router.get('/libs/get', (req, res, next) => {
  const libs = fs
    .readdirSync(libsDir)
    .filter(item => fs.lstatSync(path.join(libsDir, item)).isDirectory())
    .map(item => {
      return {
        name: path.basename(item)
      }
    })
  res.json(libs)
})

router.post('/libs/rename/:name', (req, res, next) => {
  const libName = req.params.name
  const newLibName = req.body.newName
  const libPath = path.join(libsDir, libName)
  const newLibPath = path.join(libsDir, newLibName)
  const libPngPath = path.join(libsDir, newLibName, `${libName}.png`)
  const newLibPngPath = path.join(libsDir, newLibName, `${newLibName}.png`)
  fs.renameSync(libPath, newLibPath)
  fs.renameSync(libPngPath, newLibPngPath)
  res.json({
    success: true
  })
})

router.post('/libs/delete/:name', (req, res, next) => {
  const libName = req.params.name
  const libPath = path.join(libsDir, libName)
  rimraf.sync(libPath)
  res.json({
    success: true
  })
})

module.exports = router