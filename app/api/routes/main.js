const { Router } = require('express')
const {
  openOpwMain,
  openAsana,
  openSettings,
  closeApp,
  openRadio,
  notify,
  openOpwLocal,
  openOpwCheatsheet,
} = require('../commands')

const router = Router()

router.get('/open-opw-menu', (req, res, next) => {
  openOpwMain()
  res.json({
    success: true
  })
})

router.post('/open-opw-local', (req, res, next) => {
  openOpwLocal(req.body.path)
  res.json({
    success: true
  })
})

router.post('/open-opw-cheatsheet', (req, res, next) => {
  openOpwCheatsheet(req.body.path)
  res.json({
    success: true
  })
})

router.get('/main-dev-tools', (req, res, next) => {
  nw.Window.get().showDevTools()
  res.json({
    success: true
  })
})

router.get('/open-radio', (req, res, next) => {
  openRadio()
  res.json({
    success: true
  })
})

router.get('/open-asana', (req, res, next) => {
  openAsana()
  res.json({
    success: true
  })
})

router.get('/open-settings', (req, res, next) => {
  openSettings()
  res.json({
    success: true
  })
})

router.post('/notify', (req, res, next) => {
  const text = req.body.text
  const title = req.body.title || null
  const icon = req.body.icon || null
  notify(text, title, icon)
  res.json({
    success: true
  })
})

router.post('/close-app', (req, res, next) => {
  closeApp()
  res.json({
    success: true
  })
})

module.exports = router
