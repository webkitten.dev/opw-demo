const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const rimraf = require('rimraf')
const open = require('open')
const jsonfile = require('jsonfile')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const mindmapsDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'mindmaps')

router.get('/mindmaps/get', (req, res, next) => {
  const mindmaps = fs
    .readdirSync(mindmapsDir)
    .filter(item => path.extname(item) === '.xmind')
    .map(item => {
      return {
        name: path.basename(item, '.xmind')
      }
    })
  res.json(mindmaps)
})

router.post('/mindmaps/rename/:name', (req, res, next) => {
  const mindmapName = req.params.name
  const newMindmapName = req.body.newName
  const mindmapPath = path.join(mindmapsDir, mindmapName + '.xmind')
  const newMindmapPath = path.join(mindmapsDir, newMindmapName + '.xmind')
  fs.renameSync(mindmapPath, newMindmapPath)
  res.json({
    success: true
  })
})

router.post('/mindmaps/delete/:name', (req, res, next) => {
  const mindmapName = req.params.name
  const mindmapPath = path.join(mindmapsDir, mindmapName + '.xmind')
  rimraf.sync(mindmapPath)
  res.json({
    success: true
  })
})

module.exports = router