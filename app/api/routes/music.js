const { Router } = require('express')
const YandexDisk = require('yandex-disk').YandexDisk
const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const config = require(path.join(os.homedir(), 'one-punch-web', 'config.json'))
const _ = require('lodash')
const rimraf = require('rimraf')

const router = Router()

const disk = new YandexDisk(config.yandexDisk.login, config.yandexDisk.password)

router.get('/music/disk/get', (req, res, next) => {
  const localTracks = fs.readdirSync(path.join(os.homedir(), 'one-punch-web', 'sources', 'music', 'tracks'))
  disk.readdir('./_Music/', (err, music) => {
    res.json(
      _.sortBy(
        music.filter(item => path.extname(item.displayName) === '.mp3').map(track => {
          return {
            ...track,
            name: track.displayName,
            downloading: false,
            size: (track.size / (1024*1024)).toFixed(1) + 'mb',
            local: localTracks.includes(track.displayName)
          }
        }),
        'lastModified',
      )
    )
  })
})

router.post('/music/disk/delete/:name', (req, res, next) => {
  const trackName = req.params.name
  const trackPath = path.join(os.homedir(), 'one-punch-web', 'sources', 'music', 'tracks', trackName)
  rimraf.sync(trackPath)
  disk.remove(`./_Music/${trackName}`, () => {
    res.json({
      success: true
    })
  })
})

module.exports = router