const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const open = require('open')
const rimraf = require('rimraf')
const jsonfile = require('jsonfile')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const projectsDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'projects', os.hostname())

router.get('/projects/get', (req, res, next) => {
  fs.ensureDirSync(projectsDir)
  const projects = fs
    .readdirSync(projectsDir)
    .filter(item => path.extname(item) === '.json')
    .map(item => {
      return {
        name: path.basename(item, '.json')
      }
    })
  res.json(projects)
})

router.post('/projects/open/smerge/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectJson = require(path.join(projectsDir, `${projectName}.json`))
  const command = os.platform() !== 'win32'
    ? `cd "${projectJson.path}" && smerge .`
    : `cd /d "${projectJson.path}" && smerge . && exit`
  cmd.run(command)
  res.json({
    success: true
  })
})

router.post('/projects/open/sublime/:name', (req, res, next) => {
  try {
    const projectName = req.params.name
    const projectJson = require(path.join(projectsDir, `${projectName}.json`))
    const files = projectJson.files.length ? ` -n -a ${projectJson.files.join(' -a ')}` : ''
    const command = os.platform() !== 'win32'
      ? `/bin/bash -c "cd ${projectJson.path} && subl .${files}"`
      : `cd /d "${projectJson.path}" && subl .${files} && exit`
    cmd.run(command)
  } catch (e) {
    notify(e, 'project open sublime', 'error.png')
  }
  res.json({
    success: true
  })
})

router.post('/projects/open/terminal/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectJson = require(path.join(projectsDir, `${projectName}.json`))
  // @TODO darwin command
  const command = os.platform() !== 'win32'
    ? (os.release().toLowerCase().includes('manjaro')
      ? `/bin/bash -c "xfce4-terminal --tab --drop-down --working-directory='${projectJson.path}'"`
      : `/bin/bash -c "gnome-terminal --working-directory=${projectJson.path}"`)
    : `cd /d ${projectJson.path} && conemu64.exe && exit`
  cmd.run(command)
  res.json({
    success: true
  })
})

router.post('/projects/open/folder/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectJson = require(path.join(projectsDir, `${projectName}.json`))
  open(projectJson.path)
  res.json({
    success: true
  })
})

router.post('/projects/open/vscode/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectJson = require(path.join(projectsDir, `${projectName}.json`))
  const command = os.platform() !== 'win32'
    ? `cd "${projectJson.path}" && code .`
    : `cd /d "${projectJson.path}" && code . && exit`
  cmd.run(command)
  res.json({
    success: true
  })
})

router.post('/projects/open/phpstorm/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectJson = require(path.join(projectsDir, `${projectName}.json`))
  const command = os.platform() !== 'win32'
    ? `cd "${projectJson.path}" && phpstorm .`
    : `cd /d "${projectJson.path}" && phpstorm64 . && exit`
  cmd.run(command)
  res.json({
    success: true
  })
})

router.post('/projects/rename/:name', (req, res, next) => {
  const projectName = req.params.name
  const newProjectName = req.body.newName
  const projectPath = path.join(projectsDir, projectName + '.json')
  const newProjectPath = path.join(projectsDir, newProjectName + '.json')
  fs.renameSync(projectPath, newProjectPath)
  res.json({
    success: true
  })
})

router.post('/projects/delete/:name', (req, res, next) => {
  const projectName = req.params.name
  const projectPath = path.join(projectsDir, projectName + '.json')
  rimraf.sync(projectPath)
  res.json({
    success: true
  })
})

router.post('/projects/save-project', (req, res, next) => {
  try {
    const projectName = req.body.name
    const projectPath = req.body.path
    const projectFiles = JSON.parse(req.body.files.replace(/\\\\\\\\/g, '\\\\').replace(/'/g, '"'))
    jsonfile.writeFile(path.join(projectsDir, `${projectName}.json`), {
      path: projectPath,
      files: projectFiles
    }, { spaces: 2 }, () => {
      notify(projectName, 'new project created', 'success.png')
      res.json({
        success: true
      })
    })
  } catch (e) {
    notify(e, 'project create error', 'error.png')
    res.json({
      success: false
    })
  }
})



module.exports = router