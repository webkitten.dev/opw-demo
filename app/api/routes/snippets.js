const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const parseString = require('xml2js').parseString
const cmd = require('node-cmd')
const rimraf = require('rimraf')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const snippetsPath = path.join(os.homedir(), 'one-punch-web', 'sources', 'snippets')

router.get('/snippets/get', (req, res, next) => {
  const snippets = fs
    .readdirSync(snippetsPath)
    .filter(item => path.extname(item) === '.sublime-snippet')
    .map(item => {
      return {
        name: path.basename(item, '.sublime-snippet')
      }
    })
  res.json(snippets)
})

router.get('/snippets/get/:name', (req, res, next) => {
  const snippetName = req.params.name
  const snippetPath = path.join(snippetsPath, snippetName + '.sublime-snippet')
  fs.readFile(snippetPath, 'utf8', (err, contents) => {
    parseString(contents, (err, result) => {
      const resCode = result.snippet.content[0].trim().replace(/[$]{[0-9+]:}/g, '').replace(/\\\$/g, '$')
      res.json(resCode)
    })
  })
})

router.post('/snippets/copy/:name', (req, res, next) => {
  const snippetName = req.params.name
  const snippetPath = path.join(snippetsPath, snippetName + '.sublime-snippet')
  fs.readFile(snippetPath, 'utf8', (err, contents) => {
    parseString(contents, (err, result) => {
      const resCode = result.snippet.content[0].trim().replace(/[$]{[0-9+]:}/g, '').replace(/\\\$/g, '$')
      const clipboard = nw.Clipboard.get()
      clipboard.set(resCode, 'text')
      notify(resCode, 'copied to clibboard: ', 'success.png')
      res.json({
        success: true
      })
    })
  })
})

router.post('/snippets/open/:name', (req, res, next) => {
  const newWin = req.body.newWin ? ' -n' : ''
  const snippetName = req.params.name
  const command = os.platform() !== 'win32' 
    ? `/bin/bash -c "cd ~/one-punch-web/sources/snippets && subl${newWin} -a ${snippetName}.sublime-snippet"`
    : `cd %USERPROFILE%/one-punch-web/sources/snippets && subl${newWin} -a ${snippetName}.sublime-snippet && exit`
  cmd.run(command)
  res.json({
    success: true
  })
})

router.post('/snippets/delete/:name', (req, res, next) => {
  const snippetName = req.params.name
  const snippetPath = path.join(snippetsPath, snippetName + '.sublime-snippet')
  rimraf.sync(snippetPath)
  res.json({
    success: true
  })
})

router.post('/snippets/rename/:name', (req, res, next) => {
  const snippetName = req.params.name
  const newSnippetName = req.body.newName
  const snippetPath = path.join(snippetsPath, snippetName + '.sublime-snippet')
  const newSnippetPath = path.join(snippetsPath, newSnippetName + '.sublime-snippet')
  fs.renameSync(snippetPath, newSnippetPath)
  res.json({
    success: true
  })
})

let snippetData = ''
let snippetName = ''
router.post('/snippets/save-snippet', (req, res, next) => {
  const code = req.body.code
  const name = req.body.name
  snippetData = code
  snippetName = name
  const saveSnippetWin = nw.Window.open('http://localhost:6767/save-snippet', {
    icon: './app/static/app.png',
    position: 'center',
    width: 1215,
    min_width: 800,
    height: 630,
    min_height: 630,
    show: true,
    show_in_taskbar: true,
    frame: false,
    transparent: false,
    resizable: true,
    always_on_top: true,
  })
  saveSnippetWin.focus()
  res.json({
    success: true
  })
})

router.get('/snippets/saving-snippet-data', (req, res, next) => {
  res.json({
    snippetData,
    snippetName
  })
})

router.post('/snippets/create', (req, res, next) => {
  const snippetName = req.body.name
  const code = req.body.code
  const snippetPath = path.join(snippetsPath, snippetName + '.sublime-snippet')
  fs.writeFileSync(snippetPath, code, 'utf8')
  res.json({
    success: true
  })
})

module.exports = router