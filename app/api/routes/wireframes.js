const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const cmd = require('node-cmd')
const rimraf = require('rimraf')
const open = require('open')
const jsonfile = require('jsonfile')
const { Router } = require('express')
const {
  notify,
} = require('../commands')

const router = Router()

const wireframesDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'wireframes')

router.get('/wireframes/get', (req, res, next) => {
  const wireframes = fs
    .readdirSync(wireframesDir)
    .filter(item => path.extname(item) === '.json')
    .map(item => {
      return {
        name: path.basename(item, '.json')
      }
    })
  res.json(wireframes)
})

router.post('/wireframes/rename/:name', (req, res, next) => {
  const wireframeName = req.params.name
  const newWireframeName = req.body.newName
  const wireframePath = path.join(wireframesDir, wireframeName + '.json')
  const newWireframePath = path.join(wireframesDir, newWireframeName + '.json')
  fs.renameSync(wireframePath, newWireframePath)
  res.json({
    success: true
  })
})

router.post('/wireframes/delete/:name', (req, res, next) => {
  const wireframeName = req.params.name
  const wireframePath = path.join(wireframesDir, wireframeName + '.json')
  rimraf.sync(wireframePath)
  res.json({
    success: true
  })
})

module.exports = router