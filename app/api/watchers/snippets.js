const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const watch = require('node-watch')
const _ = require('lodash')
const Q = require('q')
require('q-foreach2')(Q)

const {
  notify,
} = require('../commands')

const remoteSnippetsDir = os.platform() !== 'win32'
  ? path.join(os.homedir(), '.config', 'sublime-text-3', 'Packages', 'User')
  : path.join(process.env.APPDATA, 'Sublime Text 3', 'Packages', 'User')

const localSnippetsDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'snippets')

module.exports = {
  isWatchLocked: false,
  remoteSnippets: [],
  init() {
    watch(localSnippetsDir, (evt, name) => {
      if (!this.isWatchLocked) {
        this.isWatchLocked = true
        this.syncSnippets()
        .then(() => {
          notify('snippets synchronized', null, 'success.png')
          this.isWatchLocked = false
        })
      }
    })
  },
  syncSnippets() {
    const deferred = Q.defer()
    this.getRemoteFiles()
    .then(() => this.snippetsInstall())
    .then((items) => this.removeUnnecessarySnippets(items))
    .then(() => {
      deferred.resolve()
    })
    return deferred.promise
  },
  getRemoteFiles() {
    const deferred = Q.defer()
    const remoteFiles = fs.readdirSync(remoteSnippetsDir)
    this.remoteSnippets = remoteFiles
      .filter(item => path.extname(item) === '.sublime-snippet')
    deferred.resolve()
    return deferred.promise
  },
  snippetsInstall() {
    const deferred = Q.defer()
    fs.readdir(localSnippetsDir, (err, items) => {
      Q.forEach(items, (value) => {
        const defer = Q.defer()
        const snipp = path.join(localSnippetsDir, value)
        fs.copy(snipp, path.join(remoteSnippetsDir, value), (err) => {
          defer.resolve(value)
        })
        return defer.promise
      }).then(() => {
        deferred.resolve(items)
      })
    })
    return deferred.promise
  },
  removeUnnecessarySnippets(localSnippets) {
    const deferred = Q.defer()
    const difArr = _.difference(this.remoteSnippets, localSnippets)
    if (difArr.length !== 0) {
      Q.forEach(difArr, (value) => {
        const defer = Q.defer()
        const snipFile = path.join(remoteSnippetsDir, value)
        fs.unlink(snipFile, (err) => {
          defer.resolve(value)
        })
        return defer.promise
      }).then(function () {
        deferred.resolve()
      })
    } else {
      deferred.resolve()
    }
    return deferred.promise
  },
}