const os = require('os')
const path = require('path')
const fs = require('fs-extra')
const watch = require('node-watch')
const _ = require('lodash')
const Q = require('q')
require('q-foreach2')(Q)

const {
  notify,
} = require('../commands')

const remoteSettingsDir = os.platform() !== 'win32'
  ? path.join(os.homedir(), '.config', 'sublime-text-3', 'Packages', 'User')
  : path.join(process.env.APPDATA, 'Sublime Text 3', 'Packages', 'User')

const prefDir = os.platform() !== 'win32'
  ? path.join(os.homedir(), '.config', 'sublime-text-3', 'Packages', 'Default')
  : path.join(process.env.APPDATA, 'Sublime Text 3', 'Packages', 'Default')

const localSettingsDir = path.join(os.homedir(), 'one-punch-web', 'sources', 'sublime')

module.exports = {
  isWatchLocked: false,
  remotePlugins: [],
  remoteMacros: [],
  init() {
    watch(localSettingsDir, { recursive: true }, (evt, name) => {
      if (!this.isWatchLocked) {
        this.isWatchLocked = true
        this.syncSettings()
        .then(() => {
          notify('sublime settings synchronized', null, 'success.png')
          this.isWatchLocked = false
        })
      }
    })
  },
  syncSettings() {
    const deferred = Q.defer()
    this.getRemoteFiles()
    .then(() => this.pluginsInstall())
    .then((items) => this.removeUnnecessaryPlugins(items))
    .then(() => this.macroInstall())
    .then((items) => this.removeUnnecessaryMacro(items))
    .then(() => this.sublimeKeyMaps())
    .then(() => this.sublimeSettings())
    .then(() => this.sublimeContext())
    .then(() => {
      deferred.resolve()
    })
    return deferred.promise
  },
  getRemoteFiles() {
    const deferred = Q.defer()
    const remoteFiles = fs.readdirSync(remoteSettingsDir)
    this.remotePlugins = remoteFiles
      .filter(item => path.extname(item) === '.py')
    this.remoteMacros = remoteFiles
      .filter(item => path.extname(item) === '.sublime-macro')
    deferred.resolve()
    return deferred.promise
  },
  pluginsInstall() {
    const deferred = Q.defer()
    fs.readdir(path.join(localSettingsDir, 'plugins'), (err, items) => {
      Q.forEach(items, (value) => {
        const defer = Q.defer()
        const plugin = path.join(localSettingsDir, 'plugins', value)
        fs.copy(plugin, path.join(remoteSettingsDir, value), (err) => {
          defer.resolve(value)
        })
        return defer.promise
      }).then(() => {
        deferred.resolve(items)
      })
    })
    return deferred.promise
  },
  removeUnnecessaryPlugins(localPlugins) {
    const deferred = Q.defer()
    const difArr = _.difference(this.remotePlugins, localPlugins)
    Q.forEach(difArr, (value) => {
      const defer = Q.defer()
      const plugFile = path.join(remoteSettingsDir, value)
      fs.unlink(plugFile, (err) => {
        defer.resolve(value)
      })
      return defer.promise
    }).then(() => {
      deferred.resolve()
    })
    return deferred.promise
  },
  macroInstall() {
    const deferred = Q.defer()
    fs.readdir(path.join(localSettingsDir, 'macro'), (err, items) => {
      Q.forEach(items, (value) => {
        const defer = Q.defer()
        const macro = path.join(localSettingsDir, 'macro', value)
        fs.copy(macro, path.join(remoteSettingsDir, value), (err) => {
          defer.resolve(value)
        })
        return defer.promise
      }).then(() => {
        deferred.resolve(items)
      })
    })
    return deferred.promise
  },
  removeUnnecessaryMacro(localMacros) {
    const deferred = Q.defer()
    const difArr = _.difference(this.remoteMacros, localMacros)
    Q.forEach(difArr, (value) => {
      const defer = Q.defer()
      const macroFile = path.join(remoteSettingsDir, value)
      fs.unlink(macroFile, (err) => {
        defer.resolve(value)
      })
      return defer.promise
    }).then(() => {
      deferred.resolve()
    })
    return deferred.promise
  },
  sublimeKeyMaps() {
    const deferred = Q.defer()
    const keyMapFile = os.platform() !== 'win32'
      ? path.join(localSettingsDir, 'unix', 'Default (Linux).sublime-keymap')
      : path.join(localSettingsDir, 'win','Default (Windows).sublime-keymap')
    fs.copy(keyMapFile, path.join(remoteSettingsDir, path.parse(keyMapFile).base), (err) => {
      deferred.resolve()
    })
    return deferred.promise
  },
  sublimeSettings() {
    const deferred = Q.defer()
    let prefFile = os.platform() !== 'win32'
      ? path.join(localSettingsDir, 'unix', 'Preferences-unix.sublime-settings')
      : path.join(localSettingsDir, 'win', 'Preferences-win.sublime-settings')
    fs.copy(prefFile, path.join(prefDir, 'Preferences.sublime-settings'), (err) => {
      deferred.resolve()
    })
    return deferred.promise
  },
  sublimeContext() {
    const deferred = Q.defer()
    const contextFile = path.join(localSettingsDir, 'Context.sublime-menu')
    fs.copy(contextFile, path.join(prefDir, path.parse(contextFile).base), (err) => {
      deferred.resolve()
    })
    return deferred.promise
  },
}