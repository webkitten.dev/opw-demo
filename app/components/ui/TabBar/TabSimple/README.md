```
<template>
  <div class="SnippetsTab _tabcontent" v-loading="loading">
    <tab-simple
      :is-tab-active="isTabActive"
      :items="items"
      @leftClick="leftClick"
      @rightClick="rightClick"
      @middleClick="middleClick"
      @create="createItem"
      tab-name="snippets"
      single-item="snippet">
      <template slot="controls" slot-scope="prop">
        <i @click="renameItem(prop.item)" class="fa fa-pencil-square-o" title="rename snippet" aria-hidden="true"></i>
        <i @click="deleteItem(prop.item)" class="fa fa-trash" title="delete snippet" aria-hidden="true"></i>
      </template>
    </tab-simple>
  </div>
</template>

<script>
import TabSimple from '@/components/ui/TabBar/TabSimple/TabSimple'

export default {
  components: {
    TabSimple
  },
  props: {
    activeTab: {
      default: '',
      type: String,
    }
  },
  data() {
    return {
      items: [],
      loading: true
    }
  },
  computed: {
    isTabActive() {
      return this.$parent.active
    }
  },
  mounted() {
    this.loadItems()
    .then(() => {
      this.items = [
        { name: 'snippet1' },
        { name: 'snippet2' },
        { name: 'snippet3' },
        { name: 'snippet4' },
      ]
      this.loading = false
    })
  },
  methods: {
    loadItems() {
      return new Promise(resolve => {
        resolve()
      })
    },
    leftClick(item) {

    },
    rightClick(item) {

    },
    middleClick(item) {

    },
    createItem(item) {

    },
    deleteItem(item) {

    },
    renameItem(item) {

    }
  }
}
</script>

<style>
@import "./SnippetsTab.less";
</style>
```