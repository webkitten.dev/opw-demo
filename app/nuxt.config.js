const { NODE_ENV } = process.env

module.exports = {
  mode: 'universal',
  head: {
    title: 'One Punch Web',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
    script: [
      { src: '/js/node-context.js' },
    ],
    link: [
      { type: 'text/css', href: '/fonts/tahoma/tahoma.css' },
    ]
  },
  loading: { color: '#FFD300' },
  router: {
    middleware: 'delay'
  },
  render: {
    resourceHints: false,
  },
  server: {
    host: 'localhost',
    port: '6767',
  },
  css: [
    'element-ui/lib/theme-chalk/index.css'
  ],
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/global',
    { src: '@/plugins/vue-shortkey.js', ssr: false },
    { src: '@/plugins/vue-massonry-css.js', ssr: false },
    { src: '@/plugins/vue-drawer-layout.js', ssr: false },
    { src: '@/plugins/vue-codemirror.js', ssr: false },
    { src: '@/plugins/vue-sortable.js', ssr: false },
    { src: '@/plugins/vuedraggable.js', ssr: false },
  ],
  buildDir: NODE_ENV === 'dev' ? '.nuxt-dev' : '.nuxt',
  buildModules: [
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    baseURL: 'http://localhost:6666',
  },
  build: {
    transpile: [/^element-ui/],
    extend (config, ctx) {
    }
  }
}
