import Vue from 'vue'
import AppMenu from '@/components/ui/AppMenu/AppMenu'
import TabBar from '@/components/ui/TabBar/TabBar'

Vue.component('app-menu', AppMenu)
Vue.component('tab-bar', TabBar)