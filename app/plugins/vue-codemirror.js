import Vue from 'vue'
import VueCodemirror from 'vue-codemirror'

import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/material-darker.css'

import 'codemirror/mode/gfm/gfm.js'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/mode/vue/vue.js'
import 'codemirror/keymap/sublime.js'
import 'codemirror/addon/edit/trailingspace.js'
import 'codemirror/addon/selection/active-line.js'
import 'codemirror/addon/hint/show-hint.js'
import 'codemirror/addon/hint/anyword-hint.js'

Vue.use(VueCodemirror, {
  options: {
    theme: 'material-darker',
    mode: 'text/x-gfm',
    keyMap: 'sublime',
    lineNumbers: true,
    line: true,
    showCursorWhenSelecting: true,
    foldGutter: true,
    styleSelectedText: true,
    indentWithTabs: false,
    styleActiveLine: true,
    matchBrackets: true,
    showTrailingSpace: true,
    autofocus: false,
  }
})