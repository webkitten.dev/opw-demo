const os = require('os')
const url = require('url')
const fs = require('fs-extra')
const path = require('path')
const EventEmitter = require('events').EventEmitter
const ee = new EventEmitter()

const Promise = require('bluebird')
const rimraf = require('rimraf')
const _gitlab = require('gitlab')
const open = require('open')
const jsonfile = require('jsonfile')
const parseString = require('xml2js').parseString
const moment = require('moment'); require('moment-duration-format')(moment)
const fuzzysort = require('fuzzysort')
const _ = require('lodash')
const YandexDisk = require('yandex-disk').YandexDisk
const Q = require('q'); require('q-foreach2')(Q)
const currentPath = require('current-path')
const $ = require('jquery')
const cmd = require('node-cmd')
const marked = require('marked')
const highlightjs = require('highlight.js')
const cheerio = require('cheerio')
const xmind = require('xmind')
const watch = require('node-watch')
const pty = require('node-pty')
const shell = require('shelljs')

const TurndownService = require('turndown')

const {
  gfm,
  tables,
  strikethrough,
  taskListItems,
} = require('turndown-plugin-gfm')

const turndownService = new TurndownService({
  headingStyle: 'atx',
  bulletListMarker: '-',
  codeBlockStyle: 'fenced',
  hr: '---'
})

turndownService.addRule('summary', {
  filter: ['summary'],
  replacement: (content) => `<summary>${content}</summary>`
})

turndownService.addRule('details', {
  filter: ['details'],
  replacement: (content) => `<details>${content}</details>`
})

turndownService.use([gfm, tables, strikethrough, taskListItems])

const shellexec = (command) => new Promise(resolve => {
  if (command !== '') {
    shell.exec(command, { async: true }, () => resolve())
    return
  }
  resolve()
})

const loadedFrame = (iframe, timeout) => new Promise(resolve => {
  iframe.on('load', () => {
    setTimeout(() => {
      resolve()
    }, timeout || 0)
  })
})

const sourcesDir = path.join(os.homedir(), 'one-punch-web', 'sources')
let win = nw.Window.get()
