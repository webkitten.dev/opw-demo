var path = require('path')
var fs = require('fs')
var jsonfile = require('jsonfile')

var where = process.cwd()

var confJsonPath = path.join(where, 'shellmenu.json')

var instRegFilePath = path.join('.', 'install.reg')
var deltRegFilePath = path.join('.', 'delete.reg')

jsonfile.readFile(confJsonPath, function (err, confJson) {
  err && console.log(err)
  var instRegFileArr = [
    'Windows Registry Editor Version 5.00',
    ''
  ]
  var deltRegFileArr = [
    'Windows Registry Editor Version 5.00',
    ''
  ]
  for(var onBot in confJson['on-bot']) {
    var cli = confJson['on-bot'][onBot];
    instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cli['start'] +']')
    instRegFileArr.push('"MUIVerb"="'+ cli['MUIVerb'] +'"')
    instRegFileArr.push('"Icon"="'+ path.join(where, cli['start'], cli['start'] + ".ico").replace(/[\\]/g,'\\\\') +'"')
    instRegFileArr.push('"Position"="Middle"')
    instRegFileArr.push('')
    instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\Shell\\'+ cli['start'] +'\\command]')
    instRegFileArr.push('@="'+ path.join(where, cli['start'], cli['start'] + ".bat").replace(/[\\]/g,'\\\\') +'"')
    instRegFileArr.push('')

    deltRegFileArr.push('[-HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cli['start'] +']')
    deltRegFileArr.push('')
  }

  for(var onBot in confJson['on-top']) {
    var cli = confJson['on-top'][onBot]
    instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cli['start'] +']')
    instRegFileArr.push('"MUIVerb"="'+ cli['MUIVerb'] +'"')
    instRegFileArr.push('"Icon"="'+ path.join(where, cli['start'], cli['start'] + ".ico").replace(/[\\]/g,'\\\\') +'"')
    instRegFileArr.push('"Position"="Top"')
    instRegFileArr.push('')
    instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\Shell\\'+ cli['start'] +'\\command]')
    instRegFileArr.push('@="'+ path.join(where, cli['start'], cli['start'] + ".bat").replace(/[\\]/g,'\\\\') +'"')
    instRegFileArr.push('')

    deltRegFileArr.push('[-HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cli['start'] +']')
    deltRegFileArr.push('')
  }

  var cnt = 1;
  for(var conf in confJson){
    if(conf !== 'on-bot' && conf !== 'on-top'){
      var subCmndArr = [];
      for(var kk = 0; kk < confJson[conf].length; kk++) {
        var subCom = confJson[conf][kk]
        subCmndArr.push(subCom['start'])
        // console.log( conf ); //my-cli
        instRegFileArr.push('[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\CommandStore\\shell\\'+ subCom['start'] +']')
        instRegFileArr.push('"MUIVerb"="'+ subCom['MUIVerb'] +'"')
        instRegFileArr.push('"Icon"="'+ path.join(where, conf, subCom['start'], subCom['start'] + ".ico").replace(/[\\]/g,'\\\\') +'"')
        instRegFileArr.push('"Position"="Top"')
        instRegFileArr.push('')
        instRegFileArr.push('[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\CommandStore\\shell\\'+ subCom['start'] +'\\command]')
        instRegFileArr.push('@="'+ path.join(where, conf, subCom['start'], subCom['start'] + ".bat").replace(/[\\]/g,'\\\\') + '"')
        instRegFileArr.push('')

        deltRegFileArr.push('[-HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\CommandStore\\shell\\'+ subCom['start'] +']')
        deltRegFileArr.push('')
      }
      instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cnt + conf +']')
      instRegFileArr.push('"MUIVerb"="'+ conf +'"')
      instRegFileArr.push('"Icon"="'+ path.join(where, conf, conf + ".ico").replace(/[\\]/g,'\\\\') +'"')
      instRegFileArr.push('"Position"="Top"')
      instRegFileArr.push('"subCommands"="'+ subCmndArr.join(';') +'"')
      instRegFileArr.push('')
      instRegFileArr.push('[HKEY_CLASSES_ROOT\\Directory\\Background\\Shell\\'+ cnt + conf +'\\command]')
      instRegFileArr.push('@=""')
      instRegFileArr.push('')

      deltRegFileArr.push('[-HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\'+ cnt + conf +']')
      deltRegFileArr.push('')
      cnt++
    }
  }

  fs.writeFile(instRegFilePath, instRegFileArr.join('\r\n'), function (err) {
    if(err) console.log(err)
    console.log('install.reg ready')
  });

  fs.writeFile(deltRegFilePath, deltRegFileArr.join('\r\n'), function (err) {
    if(err) console.log(err)
    console.log('delete.reg ready')
  });

})


