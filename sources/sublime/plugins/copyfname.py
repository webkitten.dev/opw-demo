import sublime
import sublime_plugin
import os


class copyfnameCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    f = os.path.basename(self.view.window().active_view().file_name())
    sublime.set_clipboard(f)