import sublime
import sublime_plugin
from subprocess import call


class deletefileCommand(sublime_plugin.TextCommand):
  def run(self, edit, files):
    # isSure = sublime.ok_cancel_dialog('Are you sure you want to delete the file?')
    # if isSure != True:
    #   return
    import Default.send2trash as send2trash
    f = self.view.window().active_view().file_name()
    print("deleting " + f)
    send2trash.send2trash(f)
