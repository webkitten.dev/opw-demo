import sublime
import sublime_plugin


class gofileCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    v = self.view
    text = v.substr(v.sel()[0])
    sublime.active_window().run_command("show_overlay", {
      "overlay": "goto",
      "show_files": True,
      "text": text
    })