import sublime
import sublime_plugin
import re
from subprocess import Popen, PIPE
import inspect, os

class nodethisCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    f = self.view.window().active_view().file_name()
    where = os.path.dirname(f)
    fileName = os.path.basename(f)
    command = 'xterm -e bash -c \'cd '+ where +' && node '+ fileName +'; read -p "Press any key ..."\''
    if os.name == 'nt':
      command = "start cmd.exe /K \"cd /d "+ where +" && node "+ fileName +" & pause & exit\""
    Popen(command, shell=True, stdout=PIPE)


