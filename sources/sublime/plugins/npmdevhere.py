import sublime
import sublime_plugin
import re
from subprocess import Popen, PIPE
import inspect, os

class npmdevhereCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    f = self.view.window().active_view().file_name()
    where = os.path.dirname(f)
    fileName = os.path.basename(f)
    command = 'xterm -e bash -c \'cd '+ where +' && npm run dev; read -p "Press any key ..."\''
    if os.name == 'nt':
      command = "start cmd.exe /K \"cd /d "+ where +" && npm run dev && exit\""
    Popen(command, shell=True, stdout=PIPE)


