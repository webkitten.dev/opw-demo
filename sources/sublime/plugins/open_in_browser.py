import sublime, sublime_plugin

#open_in_browser

class OpenInBrowser(sublime_plugin.TextCommand):
  def run(self, edit):
    v = self.view

    page = v.substr(v.sel()[0])
    sublime.active_window().run_command('open_url', {"url": "http://" + page})