#open_npm_pack

import sublime
import sublime_plugin


class OpenNpmPack(sublime_plugin.TextCommand):
  def run(self, edit):
    v = self.view
    page = v.substr(v.sel()[0])
    if page == "":
      sublime.active_window().run_command('open_url', {"url": "https://www.npmjs.com"})
    else:
      sublime.active_window().run_command('open_url', {"url": "https://www.npmjs.com/package/" + page})
