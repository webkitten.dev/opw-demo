import sublime
import sublime_plugin
import re
from subprocess import Popen, PIPE
import inspect, os
import json

from urllib.parse import urlencode
from urllib.request import Request, urlopen


class opwCommand(sublime_plugin.TextCommand):
  def run(self, edit, _command):
    def open_opw_local():
      url = 'http://localhost:6666/open-opw-local'
      f = self.view.window().active_view().file_name()
      where = os.path.dirname(f)
      post_fields = { 'path': where }
      try:
        request = Request(url, urlencode(post_fields).encode())
        urlopen(request).read().decode()
      except Exception:
        pass

    def open_opw_cheatsheet():
      url = 'http://localhost:6666/open-opw-cheatsheet'
      f = self.view.window().active_view().file_name()
      post_fields = { 'path': f }
      try:
        request = Request(url, urlencode(post_fields).encode())
        urlopen(request).read().decode()
      except Exception:
        pass

    def save_snippet():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/snippets/save-snippet'
      post_fields = { 'code': "".join(data) }
      try:
        request = Request(url, urlencode(post_fields).encode())
        urlopen(request).read().decode()
      except Exception:
        pass

    def parse_npm_packages():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/js-parse-npm-packages'
      post_fields = { 'code': "".join(data) }
      try:
        request = Request(url, urlencode(post_fields).encode())
        urlopen(request).read().decode()
      except Exception:
        pass


    def save_project():
      v = self.view
      self.view.window().show_input_panel('project name: ', '', self.on_project_create, None, None)

    def js_beautify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/js-beautify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def js_minify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/js-minify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def js_es6_es5():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/js-es6-es5'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_less2css():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-less2css'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_autoprefix():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-autoprefix'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_remove_prefixes():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-remove-prefixes'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_beautify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-beautify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_minify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-minify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_oneline():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-oneline'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def css_px2rem():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/css-px2rem'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def html_html2pug():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/html-html2pug'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])


    def html_pug2html():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/html-pug2html'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])


    def html_cssrules():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/html-cssrules'
      post_fields = { 'code': "".join(data) }
      try:
        request = Request(url, urlencode(post_fields).encode())
        urlopen(request).read().decode()
      except Exception:
        pass


    def html_beautify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/html-beautify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])


    def html_minify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/html-minify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    def php_beautify():
      v = self.view
      data = v.substr(v.sel()[0])
      url = 'http://localhost:6666/code-processor/php-beautify'
      post_fields = { 'code': "".join(data) }
      request = Request(url, urlencode(post_fields).encode())
      response = urlopen(request).read().decode()
      result = json.loads(response)
      selection = self.view.sel()
      for region in selection:
        self.view.replace(edit, region, result['result'])

    commands = {
      'open_opw_local': open_opw_local,
      'open_opw_cheatsheet': open_opw_cheatsheet,
      'save_snippet': save_snippet,
      'save_project': save_project,
      'parse_npm_packages': parse_npm_packages,
      'js_beautify': js_beautify,
      'js_minify': js_minify,
      'js_es6_es5': js_es6_es5,
      'css_less2css': css_less2css,
      'css_autoprefix': css_autoprefix,
      'css_remove_prefixes': css_remove_prefixes,
      'css_beautify': css_beautify,
      'css_minify': css_minify,
      'css_oneline': css_oneline,
      'css_px2rem': css_px2rem,
      'html_html2pug': html_html2pug,
      'html_pug2html': html_pug2html,
      'html_cssrules': html_cssrules,
      'html_beautify': html_beautify,
      'html_minify': html_minify,
      'php_beautify': php_beautify,
    }

    commands[_command]()


  def on_project_create(self, user_input):
    url = 'http://localhost:6666/projects/save-project'
    main = sublime.active_window().folders()[0]
    opndFiles = []
    for view in sublime.active_window().views():
      opndFiles.append(view.file_name())
    post_fields = {"name": user_input, "files": opndFiles, "path": main}
    try:
      request = Request(url, urlencode(post_fields).encode())
      urlopen(request).read().decode()
      sublime.status_message('project name: ' + user_input + ' created')
    except Exception:
      pass
