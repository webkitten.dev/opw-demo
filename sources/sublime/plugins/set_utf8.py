import sublime
import sublime_plugin

class set_utf8Command(sublime_plugin.TextCommand):
  def run(self, edit):
    self.view.set_encoding('UTF-8')