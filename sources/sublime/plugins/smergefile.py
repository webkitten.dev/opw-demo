import sublime
import sublime_plugin
import re
from subprocess import Popen, PIPE
import inspect, os

class smergefileCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    f = self.view.window().active_view().file_name()
    where = os.path.dirname(f)
    fileName = os.path.basename(f)
    command = '/bin/bash -c \'cd '+ where +' && smerge log '+ fileName +'\''
    if os.name == 'nt':
      command = 'cd /d '+ where +' && smerge log '+ fileName +' && exit'
    Popen(command, shell=True, stdout=PIPE)