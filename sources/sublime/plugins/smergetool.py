import sublime
import sublime_plugin
import re
from subprocess import Popen, PIPE
import inspect, os

class smergetoolCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    print('smergetool')
    f = self.view.window().active_view().file_name()
    where = os.path.dirname(f)
    fileName = os.path.basename(f)
    command = '/bin/bash -c \'cd '+ where +' && smerge mergetool'
    if os.name == 'nt':
      command = 'cd /d '+ where +' && smerge mergetool && exit'
    Popen(command, shell=True, stdout=PIPE)