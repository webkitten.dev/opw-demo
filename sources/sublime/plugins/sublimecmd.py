import sublime
import sublime_plugin
import os
from subprocess import call

class sublime_cmdCommand(sublime_plugin.TextCommand):
  def run(self, edit):
    v = self.view
    selected = v.substr(v.sel()[0])
    if selected.strip() == "":
      f = os.path.dirname(self.view.window().active_view().file_name())
      self.view.window().show_input_panel(f + '\\', '', self.on_done, None, self.on_cancel)
    else:
      f = os.path.dirname(self.view.window().active_view().file_name())
      sublime.status_message("running command in cmd: " + selected)
      command = 'xterm -e bash -c \'cd '+ f +' && '+ selected +'; read -p "Press any key ..."\' &'
      if os.name == 'nt':
        command = 'cd /d '+ f +' && start cmd.exe @cmd /k "'+ selected +' & pause & exit" & exit'
      call(command, shell=True)

  def on_done(self, user_input):
    f = os.path.dirname(self.view.window().active_view().file_name())
    sublime.status_message("running command in cmd: " + user_input)
    command = 'xterm -e bash -c \'cd '+ f +' && '+ user_input +'; read -p "Press any key ..."\' &'
    if os.name == 'nt':
      command = 'cd /d '+ f +' && start cmd.exe @cmd /k "'+ user_input +' & pause & exit" & exit'
    call(command, shell=True)

  def on_cancel():
    sublime.status_message("Dialog canceled")